/*This is extremely exciting because this is the FIRST "thing"I have ever created
* that even remotely looks like a game.
* Created by Austin - BlueIndigo
* Date - 2/13/14 6:55 pm
*/



#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>


using namespace std;


struct sword // our sword structure
{
    int damage;
    int typenumb; // will be 1, 2, or 3, and will translaet to wood, stone, etc.
    string type; // what type of sword, etc
};

struct enemy
{
    char *name; // name of enemy
    int health = (rand()%100 + 50); // random health for enemy
};

struct character // unused for now
{
 char *name;
 int age;
 char gender;

};



int main()
{

    srand(time(0));

     sword sword1; // define a struct object
     enemy enemy1; // ^^

    enemy1.name = new char[80];

    cout << "Enter your arch-enemy's name: ";
    cin >> enemy1.name;

    sword1.typenumb = (rand()%3 + 1); // will pick a random type

    if (sword1.typenumb == 1) // if wooden, give this much possible damage, etc.
    {
        sword1.type = "wooden";
        sword1.damage = ((rand()%4 ) + 3);
    }
    else if (sword1.typenumb == 2)
    {
        sword1.type = "stone";
        sword1.damage = ((rand()%6) + 4);
    }
    else if (sword1.typenumb == 3)
    {
        sword1.type = "Mithril";
        sword1.damage = ((rand()%10) + 7);
    }

    cout << "You were given a " << sword1.type << " sword!" << endl; //display what swords each user gets
    cout << "It's damage is " << sword1.damage << endl;

    cout << "OH NO! It's your arch enemy " << enemy1.name << "!" << endl; // our enemy name
    cout << "His health is: " << enemy1.health << endl; // health of enemy

    char FIGHT; // will be a y or an n for yes or no to fight enemy1

    cout << "Strike him with your sword? y/n: ";
    cin >> FIGHT;
    if (FIGHT == 'y'){

    do{ // constantly do below
        enemy1.health = enemy1.health - ((rand()%sword1.damage) + 3); // set the enemy health to it, minus the damage done



            if (enemy1.health > 0 && enemy1.health != 0)// check if enemy health is NOT 0 or BELOW 0
            {
                cout << "Health : " << enemy1.health << endl; // if ^^ then display health
                cout << "Fight again? y/n: "; // then ask to fight again
                cin >> FIGHT; // get y or n again
            }


            if(enemy1.health < 0 || enemy1.health == 0) // if the enemy health is 0 r below 0, override the FIGHT input above ^^ with an n
            {
                FIGHT = 'n'; // force FIGHT to n so while/do loop stops
            }

    }while(FIGHT == 'y' || FIGHT != 'n'); // if n, then stop altogether

        if (enemy1.health > 0) // if you end fight before enemy is at 0 health, you fail
        {
            cout << "You failed to defeat " << enemy1.name << endl;
            delete enemy1.name; // delete the new memory of our enemy name
            return 0; // end program if they are cowards
        }


// final output

    }

    else // if you initially typed 'n' for first option
    {
        cout << "YOU COWARD" << endl;

    }

    cout << "Enemy health: 0" << endl;

    cout << "You defeated " << enemy1.name << "! Congratulations!" << endl;

    delete enemy1.name; // delete the new memory of our enemy name
    return 0;
}

