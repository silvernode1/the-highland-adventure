#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

#ifdef _WINDOWS
#include <windows.h>
#else
#include <unistd.h>
#define Sleep(x) usleep((x)*1000)
#endif

using namespace std;
string PLAYERNAME;
string INVENTORY[10]; // max of ten items!
string PLAYERCLASS = "none"; // 1 is warrior, 2 is mage, 3 is Rogue

int playerINV()
{
    // max of 10 items
    cout << "Hello, " << PLAYERNAME << ". Choose your primary weapon" << endl; // shows users name, etc.
    cout << "1. Warhammer" << endl;
    cout << "2. Staff of Elements" << endl; // the weapon the user picks defines his class then
    cout << "3. Blade of Hope" << endl;


    classpick: // here is a goto point for default case
    int firstWEAPON; // what first choice is there
    cin >> firstWEAPON;

    switch(firstWEAPON){ // possible options for the user
    case 1:{
    PLAYERCLASS = "warrior"; // become a warrior
    cout << "You've added a warhammer to your inventory" << endl;
    INVENTORY[0] = "1.Warhammer";
    break;
    }

    case 2:{
    PLAYERCLASS = "mage"; // become a mage
    cout << "You've added an Elemental staff to your inventory" << endl;
    INVENTORY[0] = "1.Elemental Staff";
    break;
    }

    case 3:{
    PLAYERCLASS = "rogue"; // become a rogue
    cout << "You've added the Blade of hope to your inventory" << endl;
    INVENTORY[0] = "1.Blade of Hope";
    break;
    }

    default:{ // in case user doesnt enter a 1-3 value
    goto classpick;
    }

    }
}

int main()
{
cout << "Honors adventurer, what is thou name: ";
cin >> PLAYERNAME; // get's user's name,

playerINV();

cout << "In your inventory currently: \n" << INVENTORY[0] << endl;
Sleep(2);

if (PLAYERCLASS == "warrior"){
    cout << "Hello there " << PLAYERNAME << ", you're going to be the mightiest warrior of all!" << endl;
    }
if (PLAYERCLASS == "mage"){
         cout << "Hello there " << PLAYERNAME << ", you're going to be the mightiest magician of all!" << endl;
    }
return 0;
}
